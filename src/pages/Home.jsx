import React, { useEffect, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Card, Col } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { fetchArticle } from '../redux/actions/articleAction';
import { Link } from "react-router-dom";
import { articleReducer } from '../redux/reducer/articleReducer';
import { deleteArticlesById } from '../utils/article.service';

export default function Home() {
  const [article, setArcticle] = useState([]);

  const history = useHistory();

  const dispatch = useDispatch()
  const state = useSelector(state => state.articleReducer.articles)

  useEffect(() => {
    dispatch(fetchArticle())
  }, [])

  async function onDeleteArticleById(id) {
    await deleteArticlesById(id);
    const temp = article.filter(item => {
        return item._id !== id
    })
    setArcticle(temp)
}

  return (
    <>
      {state.map((item, index) => (
        <Col xs="3" key={index}>
          <Card style={{ width: "15rem" }}>
            <Card.Img variant="top" width="100%" height="150px" src={item.image} />
            <Card.Body>
              <Card.Title>{item.title}</Card.Title>
              <Card.Text>{item.description}</Card.Text>
              <Link to={`/view/${item.image}`}>
                <Button variant="secondary" size="sm">View</Button>
                <Button variant="secondary" size="sm">Edit</Button>
                <Button size='sm' variant='danger' onClick={() => onDeleteArticleById(article._id)}>
                  Delete
                </Button>
              </Link>
            </Card.Body>
          </Card><br />
        </Col>
      ))}
    </>
  );
}

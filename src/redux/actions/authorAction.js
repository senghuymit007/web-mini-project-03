import authorAPI, { API } from "../../utils/authorApi";
import {authorActionType} from '../actions/authorActionType'

export const fetchAuthor= () => async dp => {

    let response = await API.get('/author');

    return dp({
        type: "FETCH_AUTHOR",
        payload: response.data.data
    });
}

export const deleteAuthorById= () => async dp => {

    let response = await API.delete('/author');

    return dp({
        type: authorActionType.DELETE_AUTHOR,
        payload: response
    });
}
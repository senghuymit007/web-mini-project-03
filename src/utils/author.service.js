import { API } from "./authorApi";

//delete Author by ID
export const deleteAuthorById = async (id) => {
    try {
        let response = await API.delete('author/' + id);
        return response.data.message;
    } catch (error) {
        console.log("deleteAuthorById Error:", error);
    }
}

export const updateAuthorById = async (id, updatedAuthor) => {
    try {
        let response = await API.put('author/' + id, updatedAuthor);
        return response.data.message;
    } catch (error) {
        console.log("deleteAuthorById Error:", error);
    }
}
import api from "./api";

//delete Author by ID
export const deleteArticlesById = async (id) => {
    try {
        let response = await api.delete('articles/' + id);
        return response.data.message;
    } catch (error) {
        console.log("deleteArticlesById Error:", error);
    }
}
